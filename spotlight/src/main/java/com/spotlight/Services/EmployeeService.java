package com.spotlight.Services;

import com.spotlight.Models.DBModels.Employee;
import com.spotlight.Models.DBModels.EmployeeRoles;
import com.spotlight.Repositories.EmployeeRepository;
import com.spotlight.Repositories.RoleRepository;
import com.spotlight.Repositories.employee_roles_repository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private employee_roles_repository employeeRolesRepository;

    @Autowired
    private RoleRepository roleRepository;
    public List<GrantedAuthority> roles_by_username(String email)
    {
        Optional<Employee> employee = employeeRepository.findByEmail(email);
        List<EmployeeRoles> roleList = employeeRolesRepository.findByEmployee(employee.get());
        List<GrantedAuthority> ret = new ArrayList<>();
        for(EmployeeRoles roleId : roleList)
        {
            ret.add(new SimpleGrantedAuthority(roleId.getRoles().getName()));
        }
        return ret;
    }

}
