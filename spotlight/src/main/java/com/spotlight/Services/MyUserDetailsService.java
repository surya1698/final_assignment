package com.spotlight.Services;

import com.spotlight.Models.AuthModels.MyUserDetails;
import com.spotlight.Models.DBModels.Employee;
import com.spotlight.Repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    EmployeeRepository userRepository;

    @Autowired
    EmployeeService employeeService;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Employee> user =  userRepository.findByEmail(s);
        List<GrantedAuthority> roles = employeeService.roles_by_username(s);
        user.orElseThrow(() -> new UsernameNotFoundException("Not Found" + s ));
        return new MyUserDetails(user.get(),roles);
    }
}
