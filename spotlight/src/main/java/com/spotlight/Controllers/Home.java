package com.spotlight.Controllers;

import com.spotlight.Models.DBModels.Corporation;
import com.spotlight.Repositories.CorpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Home {
    @Autowired
    private CorpRepository corpRepository;

    @GetMapping("/")
    public Corporation practice()
    {
        Corporation corporation = new Corporation("Nova", "Banglore");
//        corpRepository.save(corporation);
        return corporation;

    }
}
