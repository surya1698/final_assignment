package com.spotlight.Repositories;

import com.spotlight.Models.DBModels.Employee;
import com.spotlight.Models.DBModels.EmployeeRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface employee_roles_repository extends JpaRepository<EmployeeRoles,Long> {
    List<EmployeeRoles> findByEmployee(Employee employee);

}
