package com.spotlight.Repositories;

import com.spotlight.Models.DBModels.Corporation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CorpRepository extends JpaRepository<Corporation,Long> {
}
