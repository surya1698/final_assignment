package com.spotlight.Models.DBModels;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Badges {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private Boolean individual;

    private Long points;

    private Boolean isActive;

    @ManyToOne
    @JoinColumn(name = "recognition_id")
    private Recognition recognition;
}
