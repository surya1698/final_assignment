package com.spotlight.Models.DBModels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String email;

    private String password;

    private String phone;

    private String businessUnit;

    private String department;

    private Long managerId;

    private Boolean isManager;

    @OneToMany(mappedBy = "employee" )
    private Set<EmployeeRoles> roles;

    @ManyToOne@JoinColumn(name = "corporation_id")
    private Corporation corporation;
}
