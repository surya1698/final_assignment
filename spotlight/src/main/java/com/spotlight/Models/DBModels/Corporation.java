package com.spotlight.Models.DBModels;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Corporation {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String city;

    @OneToMany(mappedBy = "corporation")
    private Set<Employee> employeeList;

    @OneToMany(mappedBy = "corporation")
    private Set<Recognition> recognitionList;

    public Corporation(String name, String city) {
        this.name = name;
        this.city = city;
    }
}
