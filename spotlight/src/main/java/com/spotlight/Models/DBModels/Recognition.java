package com.spotlight.Models.DBModels;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Recognition {

    @Id
    @GeneratedValue
    private Long id;

    private String programName;

    private String description;

    private Boolean isMonetary;

    @ManyToOne
    @JoinColumn(name = "corp_id")
    private Corporation corporation;

    @OneToMany(mappedBy = "recognition")
    private List<Badges> badges_list;
}
