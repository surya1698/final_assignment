package com.spotlight;

import com.spotlight.Repositories.EmployeeRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = EmployeeRepository.class)
public class SpotlightApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotlightApplication.class, args);

	}

}
